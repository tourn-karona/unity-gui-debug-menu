# Change Log

## v1.0.0 - 2018-08-02

**Initial release**

## v1.0.1 - 2018-08-02

- Fixed issue that GUI elements are not scaled when the screen resolution is changed
- Improved debug log display