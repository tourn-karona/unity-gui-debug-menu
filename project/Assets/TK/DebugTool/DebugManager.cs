using UnityEngine;

namespace TK.DebugTool
{

	public class DebugManager : MonoBehaviour
	{
        private const float ORIGINAL_WIDTH = 480f;  // define here the original resolution
        private const float ORIGINAL_HEIGHT = 854f; // you used to create the GUI contents 
        private static DebugManager _instance = null;
        private float _sw = 0f;
        private float _sh = 0f;
        private Matrix4x4 _svMat;
        private Vector3 _guiscale = new Vector3(1f, 1f, 1f);
        private Matrix4x4 _newMatrix = new Matrix4x4();
        private float _ratio = 0f;

        private DebugMenu menu = new DebugMenu();

        private void Awake()
        {
            _ratio = ORIGINAL_HEIGHT / ORIGINAL_WIDTH;
        }

        private void OnGUI()
        {
            _svMat = GUI.matrix; // save current matrix
                                 // substitute matrix - only scale is altered from standard

            _sw = Screen.width;
            _sh = Screen.height;

            _sw = _sw < _sh ? _sw : _sh;
            _sh = _ratio * _sw;

            _guiscale.x = _sw / ORIGINAL_WIDTH;
            _guiscale.y = _sh / ORIGINAL_HEIGHT;

            _newMatrix.SetTRS(Vector3.zero, Quaternion.identity, _guiscale * 1.5f);

            GUI.matrix = _newMatrix;

            // Draw debug menu
            menu.Draw();

            // restore matrix before returning
            GUI.matrix = _svMat; // restore matrix
        }

        public static DebugManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new GameObject("_DebugManager").AddComponent<DebugManager>();
                    DontDestroyOnLoad(_instance.gameObject);
                }
                return _instance;
            }
        }

        public IDebugGroup Root
        {
            get { return menu.Root; }
        }

        public bool IsDebugOpening
        {
            get { return menu.IsDebugOpening; }
        }

        public void Close()
        {
            menu.Close();
        }
	}

}
