using UnityEngine;

namespace TK.DebugTool
{
    public class DebugLogContent : DebugItem
    {
        public class LogMessage
        {
            private string condition = "";
            private string stackTrace = "";
            private string title = "";
            private GUIStyle style = null;
            private bool hasStackTrace = false;

            private string GetRichTextLog(string text, LogType type)
            {
                string log = "";
                if (type == LogType.Error || type == LogType.Assert)
                    log += "<color=red>";
                else if (type == LogType.Warning)
                    log += "<color=yellow>";
                else
                    log += "<color=white>";
                log += text;
                log += "</color>";
                return log;
            }

            private string GetBoldRichText(string text)
            {
                return string.Format("<b>{0}</b>", text);
            }

            public LogMessage(string title, string condition, string stackTrace, LogType type)
            {
                this.title = GetBoldRichText(GetRichTextLog(title, type));
                this.condition = GetRichTextLog(condition, type);

                hasStackTrace = !string.IsNullOrEmpty(stackTrace);
                if (hasStackTrace)
                {
                    this.stackTrace = GetRichTextLog(stackTrace, type);
                }
            }

            public void Draw()
            {

                if (style == null)
                {
                    style = new GUIStyle(GUI.skin.box);
                    style.richText = true;
                    style.wordWrap = true;
                    style.alignment = TextAnchor.UpperLeft;
                }

                if (hasStackTrace)
                {
                    GUILayout.Label(string.Format("{0}\n{1}\n{2}", title, condition, stackTrace), style);
                }
                else
                {
                    GUILayout.Label(string.Format("{0}\n{1}", title, condition), style);
                }
            }
        }

        private System.Collections.Generic.List<LogMessage> logs = new System.Collections.Generic.List<LogMessage>();
        private Vector2 scroll = Vector2.zero;
        private bool showStackTrace = false;
        private bool minimize = false;
        private GUIStyle style = null;

        public DebugLogContent(string name, bool showStackTrace, string predefinedLog = "") : base(name)
        {
            logs.Add(new LogMessage(System.DateTime.Now.ToShortDateString() + " - " + System.DateTime.Now.ToShortTimeString(), predefinedLog, "", LogType.Log));
        }

        public void Prepare()
        {
            Application.logMessageReceivedThreaded += Application_logMessageReceived;
        }

        public void Release()
        {
            Application.logMessageReceivedThreaded -= Application_logMessageReceived;
        }

        private void Application_logMessageReceived(string condition, string stackTrace, LogType type)
        {
            string logTime = System.DateTime.Now.ToShortDateString() + " - " + System.DateTime.Now.ToShortTimeString();
            if (showStackTrace)
            {
                logs.Add(new LogMessage(logTime, condition, stackTrace, type));
            }
            else
            {
                logs.Add(new LogMessage(logTime, condition, "", type));
            }
        }

        public override void Draw()
        {
            if (style == null)
            {
                style = new GUIStyle(GUI.skin.textArea);
                style.richText = true;
            }

            if (minimize)
            {
                GUILayout.BeginVertical(GUILayout.Width(300));
            }
            else
            {
                GUILayout.BeginVertical(GUILayout.Width(300), GUILayout.Height(250));
            }

            GUILayout.BeginHorizontal();

            if (GUILayout.Button("Clear"))
            {
                logs.Clear();
            }
            if (GUILayout.Button(minimize ? "Maximize" : "Minimize"))
            {
                minimize = !minimize;
            }

            GUILayout.EndHorizontal();

            if (!minimize)
            {
                scroll = GUILayout.BeginScrollView(scroll, GUILayout.ExpandHeight(true), GUILayout.ExpandWidth(true));
                for (int i = 0; i < logs.Count; i++)
                {
                    logs[i].Draw();
                }
                GUILayout.EndScrollView();
            }
            GUILayout.EndVertical();
        }
    }

    public class DebugLog : DebugGroup
    {
        private DebugLogContent logContent = null;

        public DebugLog(string name, bool showStackTrace, string predefinedLog = "") : base(name)
        {
            lowOpacity = true;
            OnAccess = d =>
            {
                logContent = new DebugLogContent("Log", showStackTrace, predefinedLog);
                Add(logContent);
                logContent.Prepare();
            };

            OnExit = d =>
            {
                logContent.Release();
                Remove("Log");
            };
        }

        public DebugLog(string name, bool showStackTrace, System.Func<string> getPredefinedLog) : base(name)
        {
            lowOpacity = true;
            OnAccess = d =>
            {
                logContent = new DebugLogContent("Log", showStackTrace, getPredefinedLog());
                Add(logContent);
                logContent.Prepare();
            };

            OnExit = d =>
            {
                logContent.Release();
                Remove("Log");
            };
        }
    }
}
